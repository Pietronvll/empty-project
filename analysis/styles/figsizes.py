StandardWidth=246.0/72.27 #one PRB column
parameters_prb = {
'onecolumn' : 3.404,
'twocolumn' : 510.0/72.27,
'left_margin' : 3.404 * 0.18,
'right_margin': 3.404 * 0.04,
'bottom_margin' : 3.404 / 1.618 * 0.18,
'top_margin' : 3.404 / 1.618 * 0.04,
'cbar_width' : 3.404 / 1.618 * 0.02,
'cbar_margin' : 3.404 / 1.618 * 0.02,
'fontsize' : 10
}
parameters_nature_old = {
'onecolumn' : 3.504,
'left_margin' : 0.526,
'right_margin': 0.140,
'bottom_margin' : 0.325,
'top_margin' : 0.087,
'cbar_width' : 0.070,
'cbar_margin' : 0.070,
'fontsize' : 7
}

parameters_nature = {
'onecolumn' : 3.504,
'onehalfcolumn120' : 4.724,
'onehalfcolumn136' : 5.354,
'twocolumn' : 7.205,
'left_margin' : 0.551,
'right_margin': 0.157,
'bottom_margin' : 0.315,
'top_margin' : 0.079,
'cbar_width' : 0.079,
'cbar_margin' : 0.079,
'fontsize' : 7
}

journals = {
'PRB' : parameters_prb,
'Nature' : parameters_nature_old
}

def figsize1d(aspect_ratio = 1.618,
              journal = 'PRB',
              columns = 'onecolumn'
              ):
    '''Returns figuresize for a 1-d plot given the as_integer_ratio (x/y) of the axis.
    Usage:
    import matplotlib.pyplot as plt
    from figsize import *
    plt.figure(figsize=figsize1d())'''
    params = journals[journal]
    figure_height = ((params[columns] - params['left_margin'] - params['right_margin'])/
            aspect_ratio + params['top_margin'] + params['bottom_margin'])
    return [params[columns], figure_height]

def axes1d(aspect_ratio = 1.618,
           journal = 'PRB',
           columns = 'onecolumn'):
    '''Returns axisdimensions for a 1-d plot given the as_integer_ratio (x/y) of the axis.
    Usage:
    import matplotlib.pyplot as plt
    from figsize import *
    plt.axes(axes1d())'''
    params = journals[journal]
    figure_height = ((params[columns] - params['left_margin'] - params['right_margin'])/
            aspect_ratio + params['top_margin'] + params['bottom_margin'])
    return [params['left_margin']/params[columns],
            params['bottom_margin']/figure_height, 
            1.- (params['left_margin']+params['right_margin'])/params[columns],
            1. - (params['bottom_margin']+params['top_margin'])/figure_height]

def figsize2d(aspect_ratio = 1.618,
              journal = 'PRB',
              columns = 'onecolumn'):
    '''Same as figsize1d() for 2D plots'''
    params = journals[journal]
    figure_height = ((params[columns] - 2. * params['left_margin'] 
                      - params['cbar_margin'] -params['cbar_width'])/
            aspect_ratio + params['top_margin'] + params['bottom_margin'])
    return [params[columns],figure_height]

def axes2d(aspect_ratio = 1.618,
           journal = 'PRB',
           columns = 'onecolumn'):
    '''Same as axes1d() for 2D plots'''
    params = journals[journal]
    figure_height = ((params[columns] - 2. * params['left_margin'] 
                      - params['cbar_margin'] -params['cbar_width'])/
            aspect_ratio + params['top_margin'] + params['bottom_margin'])
    return [params['left_margin']/params[columns],
            params['bottom_margin']/figure_height, 
            1.- (2.* params['left_margin']+params['cbar_margin']+params['cbar_width'])/params[columns],
            1. - (params['bottom_margin']+params['top_margin'])/figure_height]
            
def axescb(aspect_ratio = 1.618,
           journal = 'PRB',
           columns = 'onecolumn'):
    '''Axes for the colorbar in a 2D plot
    Usage:
    plt.axes(axescb(1./np.sqrt(3)))
    '''
    params = journals[journal]
    figure_height = ((params[columns] - 2. * params['left_margin'] 
                      - params['cbar_margin'] -params['cbar_width'])/
            aspect_ratio + params['top_margin'] + params['bottom_margin'])
    return [1.-(params['left_margin']+params['cbar_width'])/params[columns],
            params['bottom_margin']/figure_height, 
            params['cbar_width']/params[columns],
            1. - (params['bottom_margin']+params['top_margin'])/figure_height]